#include "Silver.hpp"

namespace RayTracer
{
namespace Materials
{

Silver::Silver()
{
	this->RWIsReflective(true);
	this->RWReReflectiveIndex(GColor(0.14f,0.16f,0.13f));
	this->RWImReflectiveIndex(GColor(4.1f,2.3f,3.1f));
	this->RWSpecularColor(GColor(0.1f,0.1f,0.1f),0.4f);
}

Silver::~Silver()
{
}


}
}
