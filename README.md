# MySimpleRayTracer
[Project Site](https://github.com/nyari/MySimpleRayTracer) | [Wiki](https://github.com/nyari/MySimpleRayTracer/wiki) |

This is a simlpe one-way RayTracer for an older university project. It mostly uses naive and (way) less than optimal algorithms. Bit it does aim to be well strctured.
I do plan to develop and optimize it further because I find the problem interresting, and it is a really good problem to practice on.

I welcome everyone to use it for any purpose and I do welcome extensions as well, listed in the issues or otherwise.
## Current features
* Simple one way raytracing, with every added Model tested for possible intersection, no space partitioning, octal or kd-tree optimization
* Transformations by ModelView matrices
* Only thre Model implemented so far: Cube, Sphere, Plane 

## Planned features
* See Issues

## Structure

| Folder / Module | Function |
------------------|-----------
| /src/Bitmap | Bitmap output module. Credits to: Arash Partow, see in /src/Bitmap/bitmap\_image.hpp |
| /src/GraphicsBase | Basic Linear Algebraic, Trigonometric, and pixel color related classes |
| /src/MyScene | Implemetations of output types. At the moment only Bitmap is supported |
| /src/RayTracerMain | The actual implementation of the raytracing framework. |
| /src/RayTracerMaterials | Some predefined materials |
| /src/RayTracerModells | Implemented geometrical models |  
| /doc | Documentations to be added (does not exist yet) |

## Compilation
A somewhat crude CMake build environment included. The compiler has to support OpenMP

