#ifndef RAYTRACEROUTPUT_HPP
#define RAYTRACEROUTPUT_HPP

#include "World.hpp"

namespace RayTracer
{

namespace Main
{

class RayTracerOutput
{
protected:
	World* world;
public:
	RayTracerOutput(World* host);
	virtual ~RayTracerOutput();
	
	GColor GetPixelColor(int X, int Y) const;
};

}

}

#endif // RAYTRACEROUTPUT_HPP
