#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#include "GraphicsBase/GInclude.hpp"
#include "MyScene/BitmapOutput.hpp"

#include "RayTracerModells/Plane.hpp"
#include "RayTracerModells/Cube.hpp"
#include "RayTracerModells/Sphere.hpp"
#include "RayTracerMaterials/Gold.hpp"
#include "RayTracerMaterials/Silver.hpp"
#include "RayTracerMaterials/Glass.hpp"
#include "RayTracerMaterials/ProceduralXYZ.hpp"
#include "RayTracerMain/DotLight.hpp"
#include "RayTracerMain/DirectionalLight.hpp"
#include "RayTracerMain/World.hpp"

using namespace RayTracer::Modells;

using namespace GraphicsBase;
using namespace RayTracer::Main;


const unsigned int screenWidth = 1920;
const unsigned int screenHeight = 1080;

class MyWorld : public World
{
public:
	void BuildWorld()
	{
		
		eye = Eye(GPoint(19,11,0),GPoint(0,11,0), GVector(0,1,0));
		float rel = screenWidth/(float)screenHeight;
		eye.RWScreen(Screen(GSize(screenWidth,screenHeight),GVector(rel,1,0),1.4f));
		
		SetAmbiance(AmbientLight(GColor(0.0f,0.0f,0.0f)));
		/*	
		Plane* sky = new Plane(this, GPoint(0,20,0), GVector(0,-1,0));;
		Material skymat;
		skymat.RWDiffuseColor(GColor(0.78f,0.9f,0.98f));
		skymat.RWAmbientColor(skymat.RWDiffuseColor());
		sky->RWMaterial(skymat);
		AddModell(sky);
		*/
		Sphere* gomb1 = new Sphere(this,GPoint(0,0,0),1.0f);
		gomb1->RWMaterial(RayTracer::Materials::Gold());
		gomb1->RefUseModelView() = true;
		gomb1->RWModelView() *= Graphics::MakeScaleMatrix(GVector(1.5f,6,1.5f));
		//gomb1->RWModelView() *= Graphics::MakeRotateMatrix(GVector(1,0,0), GAngle(45));
		gomb1->RWModelView() *= Graphics::MakeTranslateMatrix(GVector(0,3,0));
		gomb1->GenModelViewInverse();
		AddModell(gomb1);

		Cube* kocka = new Cube(this);
		kocka->RWMaterial(RayTracer::Materials::Glass());
		kocka->RefUseModelView() = true;
		kocka->RWModelView() *= Graphics::MakeScaleMatrix(GVector(2,6,2));
		kocka->RWModelView() *= Graphics::MakeTranslateMatrix(GVector(0,3,6));
		kocka->GenModelViewInverse();
		AddModell(kocka);
		
		Sphere* gomb2 = new Sphere(this);
		gomb2->RefUseModelView() = true;
		gomb2->RWModelView() *= Graphics::MakeScaleMatrix(GVector(25, 25, 25));
		gomb2->RWModelView() *= Graphics::MakeTranslateMatrix(GVector(0, 5, 0));
		gomb2->GenModelViewInverse();
		gomb2->RWMaterial(RayTracer::Materials::Silver());
		AddModell(gomb2);
		
		Sphere* gomb3 = new Sphere(this);
		gomb3->RefUseModelView() = true;
		gomb3->RWModelView() *= Graphics::MakeScaleMatrix(GVector(5, 5, 5));
		gomb3->RWModelView() *= Graphics::MakeTranslateMatrix(GVector(0, 14, 0));
		gomb3->GenModelViewInverse();
		gomb3->RWMaterial(RayTracer::Materials::Glass());
		AddModell(gomb3);

		Sphere* gomb4 = new Sphere(this);
		Material redPlastic;
		redPlastic.RWDiffuseColor(GColor(0.5f, 0.0f, 0.0f));
		redPlastic.RWSpecularColor(GColor(0.50196078f, 0.50196078f, 0.50196078f), 1.0f);
		gomb4->RWMaterial(redPlastic);
		gomb4->RefUseModelView() = true;
		gomb4->RWModelView() *= Graphics::MakeScaleMatrix(GVector(2,2,2));
		gomb4->RWModelView() *= Graphics::MakeTranslateMatrix(GVector(-7,11,8));
		gomb4->GenModelViewInverse();
		AddModell(gomb4);
		/*
		Sphere* gomb3 = new Sphere(this,GPoint(-9,3,-4.5f),4.0f);
		gomb3->RWMaterial(RayTracer::Materials::Gold());
		AddModell(gomb3);
		
		Sphere* gomb4 = new Sphere(this,GPoint(-16,17,-4.5f),5.0f);
		gomb4->RWMaterial(RayTracer::Materials::Gold());
		AddModell(gomb4);
		
		Sphere* gomb5 = new Sphere(this, GPoint(4,5,4.0f),0.5f);
		Material gombc;
		gombc.RWDiffuseColor(GColor(0.6f,0.6f,0.1f));
		gombc.RWSpecularColor(GColor(0.3f,0.3f,0.3f),7);
		gomb5->RWMaterial(gombc);
		AddModell(gomb5);
		*/
		
		Plane* alap = new Plane(this,GPoint(0,0,0),GVector(0,1,0));
		Material grass;
		grass.RWDiffuseColor(GColor(0,0,0));
		grass.RWAmbientColor(grass.RWDiffuseColor());
		grass.RWSpecularColor(GColor(0.2f, 0.2f, 0.2f), 5.0f);
		alap->RWMaterial(grass);
		AddModell(alap);
		
		DotLight* lampa = new DotLight(this,GPoint(0,14,12),300,GColor(1,0,0),0.2f); 
		AddLight(lampa);
		
		DotLight* lampa2 = new DotLight(this,GPoint(0,14,-12),300,GColor(0,0,1),0.2f); 
		AddLight(lampa2);

		DotLight* lampa3 = new DotLight(this,GPoint(-12,14,0),300,GColor(0,1,0),0.2f); 
		AddLight(lampa3);
		//DirectionalLight* skylamp = new DirectionalLight(GVector(0.4f,-1,0.3f), GPoint(0,19.8f,0), GColor(0.8f,0.8f,0.8f));
		//AddLight(skylamp);
		
		rayLimit = 10;		
	}
	public:
	void RenderWorld()
	{
		RayTracer::MyScene::BitmapOutput kimenet(this, "result.bmp");
		kimenet.RenderToBitmap();
	}
};

void onInitialization( ) { 
	MyWorld mw;
	mw.BuildWorld();
	mw.RenderWorld();
}


int main(int argc, char **argv) {
    onInitialization();
}

