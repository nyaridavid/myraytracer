#ifndef SILVER_HPP
#define SILVER_HPP

#include "../RayTracerMain/Material.hpp" // Base class: Material

namespace RayTracer
{
namespace Materials
{

	using namespace RayTracer::Main;
	using namespace GraphicsBase;
	
class Silver : public RayTracer::Main::Material
{
public:
	Silver();
	virtual ~Silver();

};

}
}
#endif // SILVER_HPP
