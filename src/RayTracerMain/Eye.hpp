#ifndef RAYTRACERMAIN_EYE_H
#define RAYTRACERMAIN_EYE_H

namespace RayTracer
{
namespace Main
{
class Eye;
}
}

#include "../GraphicsBase/GInclude.hpp"

#include "Ray.hpp"
#include "Screen.hpp"

namespace RayTracer
{
namespace Main
{
	using namespace GraphicsBase;
	
class Eye
{
	private:
	GPoint position;
	GVector direction;
	GVector dirup;
	GVector right;
	
	Screen screen;

	void Correct();

	public:
	
	Eye()
	{
		
	}
	Eye(GPoint pos, GPoint lookat, GVector up);
	Eye(GPoint pos, GVector dir, GVector up);


	Ray ShootRay(GPoint pixel) const;

	void RWScreen(Screen screen);
	Screen RWScreen() const;
	
	void RWPosition(GPoint pos);
	GPoint RWPosition() const;

	void SetLookat(GPoint at);

	void RWDirection(GVector dir);
	GVector RWDirection() const;

	void RWUpDirection(GVector updir);
	GVector RWUpDirection() const;

};

}  // namespace RayTracerMain
}
#endif
