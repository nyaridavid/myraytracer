#ifndef DIRECTIONALLIGHT_HPP
#define DIRECTIONALLIGHT_HPP

namespace RayTracer
{
namespace Main
{
class DirectionalLight;
}
}

#include "../GraphicsBase/GInclude.hpp"

#include "Light.hpp" // Base class: RayTracerMain::Light
#include "Intersection.hpp"

namespace RayTracer
{
namespace Main
{
	using namespace GraphicsBase;
	
class DirectionalLight : public RayTracer::Main::Light
{
protected:
	GVector normal;
	GPoint planePoint;
	GColor color;
	
public:
	DirectionalLight(GVector normal, GPoint point, GColor input);
	virtual ~DirectionalLight();

public:
	virtual GVector GetLightDirection(const Intersection& intersection) const;
	virtual GColor GetLuminance(const Intersection& intersection) const;
};

}
}
#endif // DIRECTIONALLIGHT_HPP
