#ifndef AMBIENTLIGHT_HPP
#define AMBIENTLIGHT_HPP

namespace RayTracer
{
namespace Main
{
class AmbientLight;
}
}
#include "../GraphicsBase/GInclude.hpp"


namespace RayTracer
{
namespace Main
{

using namespace GraphicsBase;
	
class AmbientLight
{
	protected:
	GColor color;
public:
	AmbientLight(GColor col = GColor()):color(col)
	{
		
	}
	virtual ~AmbientLight();

public:
	virtual GColor GetLuminance() const;
};

}
}
#endif // AMBIENTLIGHT_HPP
