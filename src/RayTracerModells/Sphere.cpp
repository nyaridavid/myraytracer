#include "Sphere.hpp"

namespace RayTracer
{
namespace Modells
{
Sphere::~Sphere()
{
}

Intersection Sphere::IntersectCore(const Ray& ray) const
{
		GFloat i = ray.RWOrigin().x - center.x;
		GFloat j = ray.RWOrigin().y - center.y;
		GFloat k = ray.RWOrigin().z - center.z;
		
		GFloat a = pow(ray.RWDirection().x,2) + pow(ray.RWDirection().y,2) + pow(ray.RWDirection().z,2);
		GFloat b = 2*(i*ray.RWDirection().x + j*ray.RWDirection().y + k*ray.RWDirection().z);
		GFloat c = i*i + j*j + k*k - radius*radius;
		
		GFloat D = b*b - 4 * a * c;
		if (D < 0)
		{
			return Intersection(this,ray,0);
		} else
		{	
			bool inside;
			GFloat t = (-b - sqrt(D)) / (2*a);
			GFloat t1 = (-b + sqrt(D)) / (2*a);
			inside = false;
			if (t < 0 && t1 < 0) 
			{
				return Intersection(this,ray,0);
			}
			if (t < 0 && t1 > 0)
			{
				inside = true;
				t = t1;
			}
			GPoint pt = ray.RWOrigin() + ray.RWDirection() * t;
			GVector normal = pt - center;
			normal.normalize();
			
			return Intersection(this, ray, t, pt, normal, true, inside);
		}
}			

}
}

