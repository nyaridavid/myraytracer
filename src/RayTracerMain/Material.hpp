#ifndef RAYTRACERMAIN_MATERIAL_H
#define RAYTRACERMAIN_MATERIAL_H

namespace RayTracer
{
namespace Main
{
class Material;
}
}

#include "../GraphicsBase/GInclude.hpp"

#include "Intersection.hpp"


namespace RayTracer
{
namespace Main
{
using namespace GraphicsBase;
	
class Material
{
	protected:
	GColor n, ni; GFloat nf, nfi;
	GColor k, ki; GFloat kf, kfi;
	GColor F0, F0i; GFloat F0f, F0fi;
	
	mutable GColor ka;
	mutable GColor kd;
	mutable GColor ks;
	mutable GFloat shininess;
	bool is_reflective;
	bool is_refractive; bool is_prism;

	GColor GetRefractPrism(const Intersection& input) const;
	GColor GetRefractAvg(const Intersection& input) const;
	GVector MirrorDirection(const Intersection& input) const;
	GVector RefractDirection(const Intersection& input) const;
	void RefractPrism(const Intersection& input, GVector output[], bool ok[]) const;
	
	GColor GetN(const Intersection& input) const;
	GFloat GetNf(const Intersection& input) const;
	GColor GetK(const Intersection& input) const;
	GFloat GetKf(const Intersection& input) const;
	GColor GetF0(const Intersection& input) const;
	GFloat GetF0f(const Intersection& input) const;
	
	void SetF0();

	public:
	Material();
	virtual ~Material()
	{
		
	}
	
	GColor GetFresnelReflect(const Intersection& input) const;
	GColor GetFresnelRefract(const Intersection& input) const;
	
	virtual GColor GetAmbientColor(const Intersection& input) const;
	virtual GColor GetLocalColor(const Intersection& input) const;
	virtual GColor GetGlobalColor(const Intersection& input) const;
	virtual GColor GetReflectedColor(const Intersection& input) const;
	virtual GColor GetRefractedColor(const Intersection& input) const;
	virtual GColor GetSummedColor(const Intersection& input) const;
	
	 void RWAmbientColor(GColor input);
	 GColor RWAmbientColor() const;
	
	 void RWDiffuseColor(GColor input);
	 GColor RWDiffuseColor() const;
	
	 void RWSpecularColor(GColor input, GFloat shininess);
	 GColor RWSpecularColor() const;
	 GFloat GetShininess() const;
	 
	 void RWIsReflective(bool input);
	 bool RWIsReflective() const;
	 
	 void RWIsRefractive(bool input);
	 bool RWIsRefractive() const;
	 
	 void RWReReflectiveIndex(GColor input);
	 GColor RWReReflectiveIndex() const;
	
	 void RWImReflectiveIndex(GColor input);
	 GColor RWImReflectiveIndex() const;
	
	 void RWPrism(bool input);
	 bool PWPrism() const;
};

}  // namespace RayTracerMain
}
#endif
