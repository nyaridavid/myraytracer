#ifndef GLASS_HPP
#define GLASS_HPP

#include "../RayTracerMain/Material.hpp"

namespace RayTracer
{
namespace Materials
{
	using namespace RayTracer::Main;
	using namespace GraphicsBase;
	
class Glass : public RayTracer::Main::Material
{
public:
	Glass();
	virtual ~Glass();

};

}
}
#endif // GLASS_HPP
