#ifndef RAYTRACERMAIN_INTERSECTION_H
#define RAYTRACERMAIN_INTERSECTION_H

namespace RayTracer
{
namespace Main
{
class Intersection;
}
}

#include "../GraphicsBase/GVector.hpp"
#include "../GraphicsBase/GPoint.hpp"

#include "Modell.hpp"
#include "Ray.hpp"


namespace RayTracer
{
namespace Main
{
	using namespace GraphicsBase;
	
class Intersection
{
protected:
	const Modell* hit;
	Ray intersector;
	
	GFloat timest;
	
	GPoint point;
	GVector normal;
	GVector view;
	
	
	bool valid;
	bool inside;

	public:
	Intersection();
	Intersection(const Modell* intersected, Ray hitby, GFloat t_raydir, GPoint Point = GPoint(), GVector Normal = GVector(), bool valid = false, bool inside = false);
	
	const Modell* GetIntersected() const;
	
	void RWIntersector(Ray intersector);
	Ray RWIntersector() const;
	
	GVector GetNormal() const;
	
	void RWView(GVector vie);
	GVector RWView() const;
	
	GPoint GetIntersection() const;
	
	bool GetInside() const;
	
	bool GetValid() const;
	
	GFloat GetParamDistance() const;
	GFloat GetDistanceRaySource() const;
	
	GPoint& RefPoint() {return point;}
	GVector& RefNormal() {return normal;}
	
};

}
}
#endif
