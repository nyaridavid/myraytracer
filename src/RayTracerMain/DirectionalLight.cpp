#include "DirectionalLight.hpp"

namespace RayTracer
{
namespace Main
{
DirectionalLight::DirectionalLight(GVector normal, GPoint point, GColor input):normal(normal),planePoint(point),color(input)
{
	normal.normalize();
}

DirectionalLight::~DirectionalLight()
{
}

GVector DirectionalLight::GetLightDirection(const Intersection& intersection) const
{
	return (GFloat)(-1.0f) * normal;
}
GColor DirectionalLight::GetLuminance(const Intersection& intersection) const
{
	GPoint planep;
	GFloat K = normal * (planePoint - GPoint());
	GFloat U = normal * (intersection.GetIntersection() - GPoint());
	GFloat S = normal.x * normal.x + normal.y * normal.y + normal.z * normal.z;
	
	if (S == 0) throw normal;
	
	GFloat t = (U-K)/S;
	
	planep = intersection.GetIntersection() + t * -normal;
	
	return color * GetLightObstacles(intersection, planep); 
}

}
}

