#ifndef RAYTRACERMAIN_WORLD_H
#define RAYTRACERMAIN_WORLD_H

namespace RayTracer
{
namespace Main
{
class World;
}
}

#include <list>
#include "Eye.hpp"
#include "Light.hpp"
#include "Modell.hpp"
#include "Intersection.hpp"
#include "AmbientLight.hpp"

namespace RayTracer
{
namespace Main
{
	
	using namespace std;
	
class World
{
protected:
	
	list<Modell*> modells;
	list<Light*> lights;
	
	Eye eye;
	int rayLimit;
	
	AmbientLight ambiance;
	
protected:
	virtual void BuildWorld()
	{
		
	}

	public:
	World()
	{
		BuildWorld();
	}
	virtual ~World();

	void AddModell(Modell* input);
	void AddLight(Light* input);
	
	Intersection IntersectAllNearest(Ray ray) const;
	list<Intersection> IntersectAll(Ray ray) const;
	
	int GetRayLimit() const;
	
	const list<Light*>* GetLights() const;
	const list<Modell*>* GetModells() const;
	const AmbientLight* GetAmbiance() const
	{
		return &ambiance;
	}
	void SetAmbiance(AmbientLight input)
	{
		ambiance = input;
	}
	
	GSize GetScreenSize() const;

public:
	GColor GetPixel(int X, int Y) const;
};

}  // namespace RayTracerMain
}
#endif
