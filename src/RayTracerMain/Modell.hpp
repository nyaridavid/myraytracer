#ifndef RAYTRACERMAIN_MODELL_H
#define RAYTRACERMAIN_MODELL_H

namespace RayTracer
{
namespace Main
{
class Modell;
}
}

#include "../GraphicsBase/GInclude.hpp"

#include "Material.hpp"
#include "Ray.hpp"
#include "Intersection.hpp"
#include "World.hpp"

namespace RayTracer
{
namespace Main {
	using namespace GraphicsBase;
	
class Modell
{
protected:
	Material* material;
	World* father;
	GMatrix modelView;
	GMatrix modelViewInverse;
	bool useModelView;
	public:
	
	
	Modell(Material* material, World* father):material(material), father(father), modelView(GMatrix(4,4)), modelViewInverse(GMatrix(4,4)), useModelView(false)
	{
		modelView.LoadIdentity();
		modelViewInverse.LoadIdentity();
	}
	virtual ~Modell();

	Intersection Intersect(const Ray& ray) const;
	virtual Intersection IntersectCore(const Ray& ray) const = 0;
	
	virtual GColor GetColor(const Intersection& intersection) const;
	GColor IntersectAndGetColor(const Ray& ray) const;
	
	void RWMaterial(Material input);
	Material RWMaterial() const;
	void SetMaterial(Material* input);
	
	const GMatrix& RModelView() const;
	GMatrix& RWModelView();
	void GenModelViewInverse() 
	{
		modelViewInverse = modelView.Inversed();
	}
	
	bool& RefUseModelView()
	{
		return useModelView;
	}
	
	World* GetFather() const
	{
		return father;
	}

};

}
} 
#endif
