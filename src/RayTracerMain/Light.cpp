#include "Light.hpp"

namespace RayTracer
{
namespace Main
{
	GVector Light::GetHalfDirection(const Intersection& intersection) const
	{
		return (intersection.RWView() + GetLightDirection(intersection)).normalized();
	}
	
	GColor Light::GetLightObstacles(const Intersection& intersection, GPoint lightsource) const
	{
		list<Intersection> crossed;
		GVector lightToInt = intersection.GetIntersection() - lightsource;
		GFloat mdist = lightToInt.Length() - 0.05f;
		Ray tester(lightsource, lightToInt.normalized());
	
		crossed = intersection.GetIntersected()->GetFather()->IntersectAll(tester);
		
		GColor result(1,1,1);
		for (list<Intersection>::const_iterator i = crossed.begin(); i != crossed.end(); ++i)
		{
			if (i->GetParamDistance() < 0) continue;
			if ((i->GetIntersection() - lightsource).Length() > mdist) continue;
			if (!i->GetIntersected()->RWMaterial().RWIsRefractive())
			{
				return GColor();
			} else
			{
				GColor ref = i->GetIntersected()->RWMaterial().GetFresnelRefract(*i);
				result = ref * ref;
			} 
		}
		return result;
	}

}  // namespace RayTracerMain
}