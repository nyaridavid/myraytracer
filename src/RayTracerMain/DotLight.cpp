#include "DotLight.hpp"

namespace RayTracer
{
namespace Main
{

DotLight::DotLight(World* father, GPoint pos, GFloat pow, GColor color, GFloat a_square, GFloat a_lin, GFloat a_const):Light(),position(pos),power(pow),lightcolor(color),att_square(a_square),att_lin(a_lin),att_const(a_const)
{
}

DotLight::~DotLight()
{
}

GFloat DotLight::GetAttuneation(GFloat distance) const
{
	return 1/(pow(att_square*distance,2) + att_lin*distance + att_const);
}

GVector DotLight::GetLightDirection(const Intersection& intersection) const
{
	return (position - intersection.GetIntersection()).normalized();
}
GColor DotLight::GetLuminance(const Intersection& intersection) const
{
	GFloat distance = intersection.RWIntersector().GetDistance() + (intersection.GetIntersection() - position).Length();
	GFloat att = GetAttuneation(distance);
	
	list<Intersection> crossed;
	Ray tester(position, (intersection.GetIntersection() - position).normalized());
	
	crossed = intersection.GetIntersected()->GetFather()->IntersectAll(tester);
	
	return att * power * lightcolor * GetLightObstacles(intersection,position);
}
/*
GColor DotLight::GetRayObstacles(const list<Intersection>& input, float mdist) const
{
	GColor result(1,1,1);
	for (list<Intersection>::const_iterator i = input.begin(); i != input.end(); ++i)
	{
		if (i->GetParamDistance() < 0) continue;
		if ((i->GetIntersection() - position).Length() > mdist - 0.05f) continue;
		if (!i->GetIntersected()->RWMaterial().RWIsRefractive())
		{
			return GColor();
		} else
		{
			result *= i->GetIntersected()->RWMaterial().GetFresnelRefract(*i);
		}
	}
	return result;
}
*/
}
}
