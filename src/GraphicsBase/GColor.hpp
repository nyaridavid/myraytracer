#ifndef GRAPHICSBASE_G_COLOR_H
#define GRAPHICSBASE_G_COLOR_H

namespace Graphics
{

template<typename VAR>
class GColor
{
public:

	VAR r, g, b;

	GColor(VAR R = 0, VAR G = 0, VAR B = 0);
	
	void operator*=(const GColor<VAR>& rhs);
	GColor<VAR> operator*(const GColor<VAR>& rhs) const;

	void operator*=(VAR rhs);
	GColor<VAR> operator*(VAR rhs) const;
	
	void operator+=(const GColor<VAR>& rhs);
	GColor<VAR> operator+(const GColor<VAR>& rhs) const;
	
	void operator-=(const GColor<VAR>& rhs);
	GColor<VAR> operator-(const GColor<VAR>& rhs) const;
	
	void operator/=(const GColor<VAR>& rhs);
	GColor<VAR> operator/(const GColor<VAR>& rhs) const;
	
	void operator/=(VAR rhs);
	GColor<VAR> operator/(VAR rhs) const;
	
	VAR& operator[](int index);
	
	void normalize();
	GColor<VAR> normalized() const;
	
};
	template<typename VAR>
	GColor<VAR> operator*(VAR lhs, const GColor<VAR>& rhs);

	template<typename VAR>
	GColor<VAR>::GColor(VAR R, VAR G, VAR B):r(R),g(G),b(B)
	{
		
	}
	
	template<typename VAR>
	void GColor<VAR>::operator*=(const GColor<VAR>& rhs)
	{
		r *= rhs.r; g *= rhs.g; b *= rhs.b;
	}
	
	template<typename VAR>
	GColor<VAR> GColor<VAR>::operator*(const GColor<VAR>& rhs) const
	{
		GColor<VAR> result = *this;
		result *= rhs;
		return result;
	}
	
	template<typename VAR>
	void GColor<VAR>::operator*=(VAR rhs)
	{
		r *= rhs; g *= rhs; b *= rhs;
	}
	
	template<typename VAR>
	GColor<VAR> GColor<VAR>::operator*(VAR rhs) const
	{
		GColor<VAR> result = *this;
		result *= rhs;
		return result;
	}
	
	template<typename VAR>
	GColor<VAR> operator*(VAR lhs, const GColor<VAR>& rhs)
	{
		return rhs*lhs;
	}
	
	template<typename VAR>
	void GColor<VAR>::operator+=(const GColor<VAR>& rhs)
	{
		r += rhs.r; g += rhs.g; b += rhs.b;
	}
	
	template<typename VAR>
	GColor<VAR> GColor<VAR>::operator+(const GColor<VAR>& rhs) const
	{
		GColor<VAR> result = *this;
		result += rhs;
		return result;
	}
	
	template<typename VAR>
	void GColor<VAR>::operator-=(const GColor<VAR>& rhs)
	{
		*this += rhs * (-1);
	}
	
	template<typename VAR>
	GColor<VAR> GColor<VAR>::operator-(const GColor<VAR>& rhs) const
	{
		GColor<VAR> result = *this;
		result -= rhs;
		return result;
	}
	
	template<typename VAR>
	void GColor<VAR>::operator/=(const GColor<VAR>& rhs)
	{
		r /= rhs.r; g /= rhs.g; b /= rhs.b;
	}
	
	template<typename VAR>
	GColor<VAR> GColor<VAR>::operator/(const GColor<VAR>& rhs) const
	{
		GColor<VAR> result = *this;
		result /= rhs;
		return result;
	}
	
	template<typename VAR>
	void GColor<VAR>::operator /=(VAR rhs)
	{
		*this /= (1/rhs);
	}
	
	template<typename VAR>
	GColor<VAR> GColor<VAR>::operator/ (VAR rhs) const
	{
		GColor<VAR> result = *this;
		result /= rhs;
		return result;
	}
	
	template<typename VAR>
	VAR& GColor<VAR>::operator[](int index)
	{
		if (index == 0) return r;
		if (index == 1) return g;
		if (index == 2) return b;
		throw index;
	}
	
	template<typename VAR>
	void GColor<VAR>::normalize() {
		if (r > 1.0f) r = 1.0f;
		if (g > 1.0f) g = 1.0f;
		if (b > 1.0f) b = 1.0f;
		if (r < 0.0f) r = 0.0f;
		if (g < 0.0f) g = 0.0f;
		if (b < 0.0f) b = 0.0f;
	}
		
	template<typename VAR>
	GColor<VAR> GColor<VAR>::normalized() const {
		GColor<VAR> tmp = *this;
		tmp.normalize();
		return tmp;
	}

}  // namespace GraphicsBase
#endif
