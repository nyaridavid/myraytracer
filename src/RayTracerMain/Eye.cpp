#include "Eye.hpp"

namespace RayTracer
{
namespace Main
{
	Eye::Eye(GPoint pos, GPoint lookat, GVector up)
	{
		RWPosition(pos);
		SetLookat(lookat);
		RWUpDirection(up);
		Correct();
	}
	
	Eye::Eye(GPoint pos, GVector dir, GVector up)
	{
		RWPosition(pos);
		RWDirection(dir);
		RWUpDirection(up);
		Correct();
	}
	
	
	void Eye::RWScreen(Screen screen)
	{
		this->screen = screen;
	}
	
	Screen Eye::RWScreen() const
	{
		return screen;
	}
	
	void Eye::SetLookat(GPoint input)
	{
		GVector dir = input - position;
		if (dir.Length() == 0) return;
		RWDirection(dir.normalized());
	}
	
	void Eye::RWDirection(GVector dir)
	{
		if (dir.Length() <= 0) return;
		direction = dir.normalized();
		Correct();
	}
	
	GVector Eye::RWDirection() const
	{
		return direction;
	}
	
	void Eye::RWPosition(GPoint pos)
	{
		position = pos;
	}
	
	GPoint Eye::RWPosition() const
	{
		return position;
	}
	
	void Eye::RWUpDirection(GVector updir)
	{
		if (updir.Length() <= 0) return;
		dirup = updir.normalized();
		Correct();
	}
	
	GVector Eye::RWUpDirection() const
	{
		return dirup;
	}
	
	void Eye::Correct()
	{
		if ((direction % dirup).Length() == 0) return;
		right = direction % dirup;
		right.normalize();
		if ((right % direction).Length() == 0) return;
		dirup = right % direction;
		dirup.normalize();
	}
	
	Ray Eye::ShootRay(GPoint pixel) const
	{
		GPoint start = position;
		GPoint end = position + direction * screen.RWEyeDistance();

		GFloat normX = (pixel.x - screen.RWSize().width / 2 + 0.5f) / (screen.RWSize().width / 2);
		GFloat normY = (pixel.y - screen.RWSize().height / 2 + 0.5f) / (screen.RWSize().height / 2);

		end += right * normX * screen.RWMtp().x;
		end += dirup * normY * screen.RWMtp().y;
		
		GVector raydir = end - start;
		raydir.normalize();
		
		Ray result(start,raydir);
		return result;
	}
	
}
}