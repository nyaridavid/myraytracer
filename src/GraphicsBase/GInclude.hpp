#ifndef __GINCLUDE__
#define __GINCLUDE__

#include "GAngle.hpp"
#include "GColor.hpp"
#include "GPoint.hpp"
#include "GSize.hpp"
#include "GVector.hpp"
#include "GMatrix.hpp"
#include "GMatrixExt.hpp"

namespace GraphicsBase {
	typedef double GFloat;
	typedef Graphics::GAngle<GFloat> GAngle;
	typedef Graphics::GColor<GFloat> GColor;
	typedef Graphics::GPoint<GFloat> GPoint;
	typedef Graphics::GVector<GFloat> GVector;
	typedef Graphics::GMatrix<GFloat> GMatrix;
}

#endif
