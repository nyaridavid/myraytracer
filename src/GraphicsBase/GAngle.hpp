#ifndef GANGLE_HPP
#define GANGLE_HPP

#include <math.h>

namespace Graphics
{

template<typename ARG>
class GAngle
{

public:
	enum Type
	{
		degree,
		radian,
		gradien
	};
private:
	
	ARG angRad;
	ARG angDeg;
	ARG angGrad;
	
	Type t;
	public:
	
	
	GAngle(ARG angle = 0, Type type = degree)
	{
		switch (type)
		{
			case degree: SetDegree(angle); break;
			case radian: SetRadian(angle); break;
			case gradien: SetGradien(angle); break;
			default: throw angle; break;
		}
	}
	
	void SetDegree(ARG input)
	{
		angDeg = input;
		angRad = (angDeg / 180) * M_PI;
		angGrad = (angDeg / 180) * 200;
	}
	
	void SetRadian(ARG input)
	{
		angRad = input;
		angDeg = (angRad / M_PI) * 180;
		angGrad = (angRad / M_PI) * 200;
	}
	
	void SetGradien(ARG input)
	{
		angGrad = input;
		angDeg = (angGrad / 200) * 180;
		angRad = (angGrad / 200) * M_PI;
	}
	
	ARG GetDegree() const{
		return angDeg;
	}
	
	ARG GetRadian() const {
		return angRad;
	}
	
	ARG GetGradien() const {
		return angGrad;
	}
	
	virtual ~GAngle()
	{
	}

};

}

#endif // GANGLE_HPP
