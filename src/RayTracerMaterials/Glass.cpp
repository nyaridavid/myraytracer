#include "Glass.hpp"

namespace RayTracer
{
namespace Materials
{

Glass::Glass()
{
	this->RWPrism(false);
	this->RWIsReflective(true);
	this->RWReReflectiveIndex(GColor(1.55f,1.5f,1.45f));
	this->RWSpecularColor(GColor(0.1f,0.1f,0.1f),2);
	this->RWIsRefractive(true);
}

Glass::~Glass()
{
}


}
}
