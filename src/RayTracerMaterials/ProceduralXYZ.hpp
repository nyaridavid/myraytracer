#ifndef PROCEDURALXYZ_HPP
#define PROCEDURALXYZ_HPP

#include "../RayTracerMain/Material.hpp" // Base class: RayTracer::Main::Material
#include <math.h>

namespace RayTracer
{

namespace Materials
{
	
	using namespace RayTracer::Main;
	using namespace GraphicsBase;

class ProceduralXYZ : public RayTracer::Main::Material
{
public:
	GColor omega;
	GColor fi;
	GColor mtp;
	
public:
	ProceduralXYZ(GColor w, GColor f, GColor m):omega(w), fi(f), mtp(m)
	{
	}
	virtual ~ProceduralXYZ()
	{
	}

public:
	virtual GColor GetLocalColor(const Intersection& input) const
	{
		GPoint tmp = input.GetIntersection();
		kd.r = 	fabs(sin(omega.r * tmp.x + fi.r) +
				   sin(omega.r * tmp.y + fi.r) +
				   sin(omega.r * tmp.z + fi.r)) / 3;
		kd.g =  fabs(sin(omega.g * tmp.x + fi.g) +
				   sin(omega.g * tmp.y + fi.g) +
				   sin(omega.g * tmp.z + fi.g)) / 3;
		kd.b =  fabs(sin(omega.b * tmp.x + fi.b) +
				   sin(omega.b * tmp.y + fi.b) +
				   sin(omega.b * tmp.z + fi.b)) / 3;
		kd *= mtp;
		kd.normalize();
		
		return Material::GetLocalColor(input);
	}
};

}

}

#endif // PROCEDURALXYZ_HPP
