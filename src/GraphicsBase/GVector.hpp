#ifndef GRAPHICSBASE_G_VECTOR_H
#define GRAPHICSBASE_G_VECTOR_H

#define _USE_MATH_DEFINES
#include <math.h>
#include "GAngle.hpp"

namespace Graphics
{
template<typename VAR>
class GVector
{
	private:
	static VAR equ;
	
	public:
	
	VAR x, y, z;
	GVector(VAR x0 = 0, VAR y0 = 0, VAR z0 = 0);
	
	bool operator==(const GVector<VAR>& rhs) const;
	static void SetEquTolerance(VAR input);
	static VAR GetEquTolerance();
	
	
	void operator*=(const VAR rhs);
	GVector<VAR> operator*(const VAR rhs) const;
	void operator/=(VAR rhs);
	GVector<VAR> operator/(const VAR rhs) const;
	
	void operator+=(const GVector<VAR>& rhs);
	GVector<VAR> operator+(const GVector<VAR>& rhs) const;
	void operator-=(const GVector<VAR>& rhs);
	GVector<VAR> operator-(const GVector<VAR>& rhs) const;
	GVector<VAR> operator-() const;
	
	VAR operator*(const GVector<VAR>& rhs) const;
	
	void operator%=(const GVector<VAR>& rhs);
	GVector<VAR> operator%(const GVector<VAR>& rhs) const;
	
	VAR& operator[](int index);
	const VAR& operator[](int index) const;
	
	VAR Length() const;

	VAR cosAlpha(const GVector<VAR>& v) const;
	GAngle<VAR> arc(const GVector<VAR>& v) const;
	GVector<VAR> normalized() const;
	void normalize();

};

template<typename VAR>
VAR GVector<VAR>::equ = 0;

template<typename VAR>
bool GVector<VAR>::operator==(const GVector<VAR>& rhs) const
{
	VAR dif;
	
	dif = x - rhs.x; if (dif < 0) dif = -dif;
	if (dif > equ) return false;
	dif = y - rhs.y; if (dif < 0) dif = -dif;
	if (dif > equ) return false;
	dif = z - rhs.z; if (dif < 0) dif = -dif;
	if (dif > equ) return false;
	return true;
}

template<typename VAR>	
void GVector<VAR>::SetEquTolerance(VAR input)
{
	if (input < 0) return;
	equ = input;
}

template<typename VAR>
VAR GVector<VAR>::GetEquTolerance()
{
	return equ;
}

template<typename VAR>
GVector<VAR> operator* (VAR lhs, const GVector<VAR>& rhs);

template<typename VAR>
GVector<VAR>::GVector(VAR x0, VAR y0, VAR z0):x(x0),y(y0),z(z0) 
{
	
}

template<typename VAR>
void GVector<VAR>::operator *=(VAR rhs)
{
	x *= rhs; y *= rhs; z *= rhs;
}

template<typename VAR>
GVector<VAR> GVector<VAR>::operator*(VAR rhs) const
{
	GVector<VAR> result = *this;
	result *= rhs;
	return result;
}

template<typename VAR>
GVector<VAR> operator* (VAR lhs, const GVector<VAR>& rhs)
{
	return rhs * lhs;
}

template<typename VAR>
void GVector<VAR>::operator /=(VAR rhs)
{
	*this *= 1/rhs;
}

template<typename VAR>
GVector<VAR> GVector<VAR>::operator /(VAR rhs) const
{
	GVector<VAR> result = *this;
	result /= rhs;
	return result;
}

template<typename VAR>
void GVector<VAR>::operator+=(const GVector<VAR>& rhs)
{
	x += rhs.x; y += rhs.y; z += rhs.z;
}

template<typename VAR>
GVector<VAR> GVector<VAR>::operator+(const GVector<VAR>& rhs) const
{
	GVector<VAR> result = *this;
	result += rhs;
	return result;
}

template<typename VAR>
void GVector<VAR>::operator-=(const GVector<VAR>& rhs)
{
	x -= rhs.x; y -= rhs.y; z -= rhs.z;
}

template<typename VAR>
GVector<VAR> GVector<VAR>::operator-(const GVector<VAR>& rhs) const
{
	GVector<VAR> result = *this;
	result -= rhs;
	return result;
}

template<typename VAR>
GVector<VAR> GVector<VAR>::operator-() const
{
	return (VAR)(-1.0) * *this;
}

template<typename VAR>
VAR GVector<VAR>::operator*(const GVector<VAR>& rhs) const {
	return (x * rhs.x + y * rhs.y + z * rhs.z);
}

template<typename VAR>
void GVector<VAR>::operator%=(const GVector<VAR>& rhs)
{
	VAR xt = y*rhs.z-z*rhs.y;
	VAR yt = z*rhs.x-x*rhs.z;
	VAR zt = x*rhs.y-y*rhs.x;
	x = xt; y = yt; z = zt;
}

template<typename VAR>
GVector<VAR> GVector<VAR>::operator%(const GVector<VAR>& rhs) const
{
	GVector<VAR> result = *this;
	result %= rhs;
	return result;
}

template<typename VAR>
VAR& GVector<VAR>::operator[](int index)
{
	if (index == 0) return x;
	if (index == 1) return y;
	if (index == 2) return z;
	throw index;
}

template<typename VAR>
const VAR& GVector<VAR>::operator[](int index) const
{
	if (index == 0) return x;
	if (index == 1) return y;
	if (index == 2) return z;
	throw index;
}

template<typename VAR>
VAR GVector<VAR>::Length() const {
	return sqrt(x * x + y * y + z * z);
}

template<typename VAR>
VAR GVector<VAR>::cosAlpha(const GVector<VAR>& v) const {
	return (*this * v) / (Length() * v.Length());
}

template<typename VAR>
GAngle<VAR> GVector<VAR>::arc(const GVector<VAR>& v) const {
	return GAngle<VAR>(acos(cosAlpha(v)), Graphics::GAngle<VAR>::radian);
}

template<typename VAR>
GVector<VAR> GVector<VAR>::normalized() const {
	return *this * (1/Length());
}

template<typename VAR>
void GVector<VAR>::normalize() {
	*this = *this * (1/Length());
}

}  // namespace GraphicsBase
#endif
