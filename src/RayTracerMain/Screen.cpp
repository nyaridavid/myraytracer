#include "Screen.hpp"

namespace RayTracer
{
namespace Main
{
	Screen::Screen()
	{
		RWEyeDistance(1);
		RWMtp(GVector(1,1,0));
		RWSize(GSize(600,600));
	}
	
	Screen::Screen(GSize insize, GVector inmtp, GFloat eyed)
	{
		RWEyeDistance(eyed);
		RWMtp(inmtp);
		RWSize(insize);
	}
	
	void Screen::RWMtp(GVector input)
	{
		mtp = input;
	}
	
	GVector Screen::RWMtp() const
	{
		return mtp;
	}
	
	void Screen::RWSize(GSize input)
	{
		size = input;
	}
	
	GSize Screen::RWSize() const
	{
		return size;
	}
	
	void Screen::RWEyeDistance(GFloat input)
	{
		if (input > 0.0f) eyedist = input;
	}
	
	GFloat Screen::RWEyeDistance() const
	{
		return eyedist;
	}

}
}