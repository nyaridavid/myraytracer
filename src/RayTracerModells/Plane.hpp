#ifndef PLANE_HPP
#define PLANE_HPP

#include "../GraphicsBase/GPoint.hpp"
#include "../GraphicsBase/GVector.hpp"

#include "../RayTracerMain/Modell.hpp" // Base class: RayTracerMain::Modell
#include "../RayTracerMain/Intersection.hpp"
#include "../RayTracerMain/Ray.hpp"

namespace RayTracer
{
namespace Modells
{
	using namespace GraphicsBase;
	using namespace RayTracer::Main;
	
class Plane : public RayTracer::Main::Modell
{
	GPoint base;
	GVector normal;
	
public:
	Plane(World* father, GPoint given, GVector n):Modell(new Material(),father),base(given),normal(n)
	{

	}
	virtual ~Plane();

public:
	virtual Intersection IntersectCore(const Ray& ray) const;
};

}
}
#endif // PLANE_HPP
