#include "Gold.hpp"

namespace RayTracer
{
namespace Materials
{

Gold::Gold()
{
	this->RWIsReflective(true);
	this->RWReReflectiveIndex(GColor(0.17f,0.35f,1.5f));
	this->RWImReflectiveIndex(GColor(3.1f,2.7f,1.9f));
	this->RWSpecularColor(GColor(0.1f,0.1f,0.1f),0.4f);
}	
Gold::~Gold()
{
}


}
}
