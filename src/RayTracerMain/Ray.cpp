#include "Ray.hpp"

namespace RayTracer
{
namespace Main
{
	Ray::Ray(GPoint start, GVector direction)
	{
		RWOrigin(start);
		RWDirection(direction.normalized());
		depht = 1;
		distance = 0;
	}
	
	Ray Ray::Shoot() const
	{
		Ray copy = *this;
		++copy.depht;
		return copy;
	}
	Ray Ray::Shoot(GPoint start, GVector direction) const
	{
		Ray copy = Shoot();
		copy.start = start + direction * 0.005f;
		copy.direction = direction;
		return copy;
	}
	Ray Ray::Impact(GPoint impactpoint) const
	{
		Ray copy = *this;
		copy.distance += (impactpoint - start).Length();
		return copy;
	}
	
	void Ray::RWOrigin(GPoint input)
	{
		start = input;
	}
	GPoint Ray::RWOrigin() const
	{
		return start;
	}
	void Ray::RWDirection(GVector input)
	{
		if (input.Length() == 0) return;
		direction = input;
	}
	GVector Ray::RWDirection() const
	{
		return direction;
	}
	
	int Ray::GetDepht() const
	{
		return depht;
	}
	GFloat Ray::GetDistance() const
	{
		return distance;
	}

}
}