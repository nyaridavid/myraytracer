#ifndef RAYTRACERMAIN_SCREEN_H
#define RAYTRACERMAIN_SCREEN_H

namespace RayTracer
{
namespace Main
{
class Screen;
}
}

#include "../GraphicsBase/GInclude.hpp"

namespace RayTracer
{
namespace Main
{
	using namespace GraphicsBase;
	
class Screen
{

	private:
	
	GFloat eyedist;
	GSize size;
	GVector mtp;
	
	public:
	
	
	Screen();
	Screen(GSize insize, GVector inmtp, GFloat eyed);
	
	void RWEyeDistance(GFloat input);
	GFloat RWEyeDistance() const;
	
	void RWSize(GSize input);
	GSize RWSize() const;
	
	void RWMtp(GVector input);
	GVector RWMtp() const;
};

}
}
#endif
