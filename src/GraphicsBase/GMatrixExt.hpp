#ifndef GMATRIXEXT_HPP
#define GMATRIXEXT_HPP

#include "GVector.hpp"
#include "GPoint.hpp"
#include "GAngle.hpp"
#include <math.h>

namespace Graphics
{

template<typename VAR>
GMatrix<VAR> MakeTranslateMatrix(const GVector<VAR>& input)
{
	GMatrix<VAR> result(4,4);
	result.LoadIdentity();
	for (int i = 0; i < 3; ++i)
	{
		result.RWData(3,i,input[i]);
	}
	return result;
}

template<typename VAR>
GMatrix<VAR> MakeScaleMatrix(const GVector<VAR>& input)
{
	GMatrix<VAR> result(4,4);
	result.LoadIdentity();
	for (int i = 0; i < 3; ++i)
	{
		result.RWData(i,i,input[i]);
	}
	return result;
}

template<typename VAR>
GMatrix<VAR> MakeRotateMatrix(GVector<VAR> around, const GAngle<VAR>& by)
{
	GMatrix<VAR> result(4,4);
	VAR c = cos(by.GetRadian());
	VAR s = sin(by.GetRadian());
	around.normalize();
	result.LoadIdentity();
	for (int i = 0; i < 3; ++i)
	{
		VAR ar2 = around[i] * around[i];
		result.RWData(i,i,c*(1-ar2) + ar2);
	}
	VAR c2 = 1-c;
	result.RefData(1,0) = around.x * around.y * c2 - around.z * s;
	result.RefData(2,0) = around.x * around.z * c2 + around.y * s;
	result.RefData(0,1) = around.y * around.x * c2 + around.z * s;
	result.RefData(2,1) = around.y * around.z * c2 - around.x * s;
	result.RefData(0,2) = around.x * around.z * c2 - around.y * s;
	result.RefData(1,2) = around.y * around.z * c2 + around.x * s;
	
	return result.Inversed();
}

template<typename VAR>
GMatrix<VAR> GVectorToRowMatrix(const GVector<VAR>& input)
{
	GMatrix<VAR> result(1,4);
	result.RWData(0,3,0);
	for (int i = 0; i < 3; ++i)
		result.RWData(0,i,input[i]);
	return result;
}

template<typename VAR>
GMatrix<VAR> GVectorToColumnMatrix(const GVector<VAR>& input)
{
	GMatrix<VAR> result(4,1);
	result.RWData(3,0,0);
	for (int i = 0; i < 3; ++i)
		result.RWData(i,0,input[i]);
	return result;
}

template<typename VAR>
GVector<VAR> GMatrixToVector(const GMatrix<VAR>& input)
{
	if (input.GetN() == 4 && input.GetM() == 1)
	{
		GVector<VAR> result;
		for (int i = 0; i < 3; ++i)
		{
			result[i] = input.RWData(i,0);
		}
		return result;
	}
	if (input.GetN() == 1 && input.GetM() == 4)
	{
		GVector<VAR> result;
		for (int i = 0; i < 3; ++i)
		{
			result[i] = input.RWData(0,i);
		}
		return result;
	}
	throw input;
}

template<typename VAR>
GMatrix<VAR> GPointToRowMatrix(const GPoint<VAR>& input)
{
	GMatrix<VAR> result(1,4);
	result.RWData(0,3,1);
	for (int i = 0; i < 3; ++i)
		result.RWData(0,i,input[i]);
	return result;
}

template<typename VAR>
GMatrix<VAR> GPointToColumnMatrix(const GPoint<VAR>& input)
{
	GMatrix<VAR> result(4,1);
	 result.RWData(3,0,1);
	 for (int i = 0; i < 3; ++i)
		 result.RWData(i,0,input[i]);
	return result;
}

template<typename VAR>
GPoint<VAR> GMatrixToPoint(const GMatrix<VAR>& input)
{
	if (input.GetN() == 4 && input.GetM() == 1)
	{
		GPoint<VAR> result;
		VAR divider = input.RWData(3,0);
		for (int i = 0; i < 3; ++i)
		{
			result[i] = input.RWData(i,0) / divider;
		}
		return result;
	}
	if (input.GetN() == 1 && input.GetM() == 4)
	{
		GPoint<VAR> result;
		VAR divider = input.RWData(0,3);
		for (int i = 0; i < 3; ++i)
		{
			result[i] = input.RWData(0,i) / divider;
		}
		return result;
	}
	throw input;
}

}


#endif // GMATRIXEXT_HPP
