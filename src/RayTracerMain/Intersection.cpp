#include "Intersection.hpp"

namespace RayTracer
{
namespace Main
{
	Intersection::Intersection():intersector(GPoint(),GVector())
	{
		valid = false;
		hit = 0;
	}

	Intersection::Intersection(const Modell* intersected, Ray hitby, GFloat t_raydir, GPoint Point, GVector Normal, bool valid, bool inside):hit(intersected),intersector(hitby),timest(t_raydir), point(Point),normal(Normal),valid(valid),inside(inside)
	{
		normal.normalize();
	}
	
	void Intersection::RWIntersector(Ray intersector)
	{
		this->intersector = intersector;
	}
	Ray Intersection::RWIntersector() const
	{
		return intersector;
	}
	
	void Intersection::RWView(GVector vie)
	{
		view = vie;
	}
	
	GVector Intersection::RWView() const
	{
		return view;
	}
	
	bool Intersection::GetValid() const
	{
		return valid;
	}
	
	bool Intersection::GetInside() const
	{
		return inside;
	}
	
	GFloat Intersection::GetParamDistance() const
	{
		return timest;
	}
	
	GFloat Intersection::GetDistanceRaySource() const
	{
		return (point - intersector.RWOrigin()).Length();
	}
	
	const Modell* Intersection::GetIntersected() const
	{
		return hit;
	}
	
	GVector Intersection::GetNormal() const
	{
		if (!inside) return normal; else return -1.0 * normal;
	}
	
	GPoint Intersection::GetIntersection() const
	{
		return point;
	}

}
}
