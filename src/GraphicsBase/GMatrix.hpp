#ifndef GMATRIX_HPP
#define GMATRIX_HPP

#include <utility>


namespace Graphics
{
template <typename VAR>
class GMatrix
{
private:
	int N, M;
	VAR* data;

private:
	void ReserveData();
	void FreeData();
	void TestInput() const;
	void TestInput(const GMatrix& input) const;
	void TestInput(const GMatrix& lhs, const GMatrix& rhs) const;
	
public:
	GMatrix(int n, int m);
	GMatrix(const GMatrix& input);
	GMatrix(GMatrix&& rvalue);
	const GMatrix& operator=(const GMatrix& rhs);
	const GMatrix& operator=(GMatrix&& rvalue);
	virtual ~GMatrix();
	
	const VAR& RWData(int i, int j) const;
	void RWData(int i, int j, const VAR& input);
	VAR& RefData(int i, int j);
	
	int GetN() const { return N; }
	int GetM() const { return M; }

public:
	void operator+= (const GMatrix& rhs);
	GMatrix operator+ (const GMatrix& rhs) const;
	
	void operator-= (const GMatrix& rhs);
	GMatrix operator- (const GMatrix& rhs) const;
	
	void operator *= (const GMatrix& rhs);
	GMatrix operator* (const GMatrix& rhs) const;
	void operator*= (const VAR& rhs);
	GMatrix operator* (const VAR& rhs) const;
	
	GMatrix PartialMatrix(int i, int j) const;
	
	VAR Determinant() const;
	GMatrix Adjunged() const;
	void Adjung();
	GMatrix Transposed() const;
	void Transpose();
	GMatrix Inversed() const;
	void Inverse();
	
	void LoadIdentity();
};

template<typename VAR>
void GMatrix<VAR>::ReserveData()
{
	data = new VAR[N*M];
}

template<typename VAR>
void GMatrix<VAR>::FreeData()
{
	if (data != nullptr)
		delete[] data;
}

template<typename VAR>
void GMatrix<VAR>::TestInput() const
{
	if (N == M)
		return;
	else
		throw *this;
}

template<typename VAR>
void GMatrix<VAR>::TestInput(const GMatrix<VAR>& input) const
{
	if (N == input.N && M == input.M)
		return;
	else
		throw input;
}

template<typename VAR>
void GMatrix<VAR>::TestInput(const GMatrix<VAR>& lhs, const GMatrix<VAR>& rhs) const
{
	if (lhs.M == rhs.N)
		return;
	else
		throw *this;
}

template<typename VAR>
GMatrix<VAR>::GMatrix(int n, int m):N(n),M(m) 
{
	ReserveData();
}

template<typename VAR>
GMatrix<VAR>::GMatrix(const GMatrix& input):N(input.N), M(input.M) 
{
	ReserveData();
	int size = N * M;
	for (int i = 0; i < size; ++i)
	{
		data[i] = input.data[i];
	}
}

template<typename VAR>
GMatrix<VAR>::GMatrix(GMatrix&& rvalue):N(rvalue.N), M(rvalue.M), data(rvalue.data)
{
	rvalue.data = nullptr;
}

template<typename VAR>
const GMatrix<VAR>& GMatrix<VAR>::operator=(const GMatrix& rhs)
{
	if (N != rhs.N || M != rhs.M)
	{
		FreeData();
		N = rhs.N; M = rhs.M;
		ReserveData();
	}

	int size = N * M;
	for (int i = 0; i < size; ++i)
	{
		data[i] = rhs.data[i];
	}
	return rhs;
}


template<typename VAR>
const GMatrix<VAR>& GMatrix<VAR>::operator=(GMatrix&& rvalue)
{
	N = rvalue.N;
	M = rvalue.M;
	data = rvalue.data;
	rvalue.data = nullptr;
	return *this;
}

template<typename VAR>
GMatrix<VAR>::~GMatrix() 
{
	FreeData();
}

template<typename VAR>
const VAR& GMatrix<VAR>::RWData(int i, int j) const
{
	if ((i >= 0 && i < N) && (j >= 0 && j < M))
		return data[i*N + j];
	else
		throw i ^ j;
}

template<typename VAR>
void GMatrix<VAR>::RWData(int i, int j, const VAR& input)
{
	if ((i >= 0 && i < N) && (j >= 0 && j < M))
		data[i*N + j] = input;
	else
		throw i ^ j;
}

template<typename VAR>
VAR& GMatrix<VAR>::RefData(int i, int j)
{
	if ((i >= 0 && i < N) && (j >= 0 && j < M))
		return data[i*N + j];
	else
		throw i ^ j;
}

template<typename VAR>
void GMatrix<VAR>::operator+= (const GMatrix<VAR>& rhs)
{
	TestInput(rhs);
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			data[i*N + j] += rhs.data[i*N + j];
		}
	}
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::operator+ (const GMatrix<VAR>& rhs) const 
{
	TestInput(rhs);
	GMatrix result(*this);
	result += rhs;
	return result;
}

template<typename VAR>
void GMatrix<VAR>::operator-= (const GMatrix<VAR>& rhs)
{
	TestInput(rhs);
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			data[i*N + j] -= rhs.data[i*N + j];
		}
	}
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::operator- (const GMatrix<VAR>& rhs) const
{
	TestInput(rhs);
	GMatrix result(*this);
	result -= rhs;
	return result;
}
template<typename VAR>
void GMatrix<VAR>::operator*= (const GMatrix<VAR>& rhs)
{
	*this = GMatrix<VAR>(*this) * rhs;
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::operator* (const GMatrix<VAR>& rhs) const
{
	TestInput(*this,rhs);
	GMatrix result(N, rhs.M);
	for (int iR = 0; iR < result.N; ++iR)
	{
		for (int jR = 0; jR < result.M; ++jR)
		{
			result.data[iR*result.N + jR] = 0;
			for (int k = 0; k < M; ++k)
			{
				result.data[iR*result.N + jR] += data[iR*result.N + k] * rhs.data[k*M + jR];
			}
		}
	}
	return result;
}

template<typename VAR>
void GMatrix<VAR>::operator*= (const VAR& rhs)
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			data[i*N + j] *= rhs;
		}
	}
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::operator* (const VAR& rhs) const
{
	GMatrix result(*this);
	result *= rhs;
	return result;
}

template<typename VAR>
GMatrix<VAR> operator* (const VAR& lhs, const GMatrix<VAR>& rhs)
{
	return rhs * lhs;
}
	
template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::PartialMatrix(int i, int j) const
{
	GMatrix result(N-1,M-1);
	
	int iO = 0, jO = 0;
	
	for (int I = 0; I < N; ++I)
	{
		if (i == I)
			{iO = 1; continue;}
		for (int J = 0; J < M; ++J)
		{
			if (j == J)
				{jO = 1; continue;}
			int x = I-iO;
			int y = J-jO;
			VAR res = data[I*N + J];
			result.data[x*(N-1) + y] = res;
		}
		jO = 0;
	}
	
	return result;
}

template<typename VAR>
VAR GMatrix<VAR>::Determinant() const
{
	TestInput();
	if (N == 1)
		return data[0];
	VAR result = 0;
	for (int i = 0; i < N; ++i)
	{
		VAR det = PartialMatrix(i,0).Determinant();
		result += ((i % 2 == 0) ? 1 : -1) * det * data[i*N];
	}
	return result;
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::Adjunged() const
{
	TestInput();
	GMatrix result(N, M);
	for (int j = 0; j < M; ++j)
	{
		for (int i = 0; i < N; ++i)
		{
			VAR adj = PartialMatrix(i,j).Determinant();
			result.data[j*M + i] = adj;
		}
	}
	return result;
}

template<typename VAR>
void GMatrix<VAR>::Adjung()
{
	*this = Adjunged();
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::Transposed() const
{
	GMatrix result(M, N);
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			result.data[j*M + i] = data[i*N + j];
		}
	}
	return result;
}

template<typename VAR>
void GMatrix<VAR>::Transpose()
{
	*this = Transposed();
}

template<typename VAR>
GMatrix<VAR> GMatrix<VAR>::Inversed() const
{
	return Adjunged() * (1.0 / Determinant());
}

template<typename VAR>
void GMatrix<VAR>::Inverse()
{
	*this = Inversed();
}

template<typename VAR>
void GMatrix<VAR>::LoadIdentity()
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			data[i*N + j] = (i == j) ? 1 : 0;
		}
	}
}

}
#endif // GMATRIX_HPP
