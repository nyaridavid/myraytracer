#include "Cube.hpp"

#include <limits>

namespace RayTracer
{
namespace Modells
{
template<typename T>
static bool Between(T greaterThan, T value, T lessThan) {
	if (greaterThan < value && value < lessThan)
		return true;
	else
		return false;	
}
static bool GetRayTravel(GFloat p0, GFloat dp, GFloat& t, GFloat& cross) {
	if (dp == 0.0f)
		return false;
	GFloat t1 = (0.5f - p0) / dp;
	GFloat t2 = (-0.5f - p0) / dp;

	if (t1 > 0 && t2 > 0) {
		if (t1 < t2) {
			t = t1;
			cross = p0 + t1 * dp;
		} else {
			t = t2;
			cross = p0 + t2 * dp;
		}
	} else if (t1 > 0) {
		t = t1;
		cross = p0 + t1 * dp;
	} else if (t2 > 0) {
		t = t2;
		cross = p0 + t2 * dp;
	} else {
		return false;
	}

	return true;
}

Cube::~Cube()
{
}

Intersection Cube::IntersectCore(const Ray& ray) const
{
		const GPoint& origin = ray.RWOrigin();
		const GVector& dir = ray.RWDirection();

		int crossedPlane = -1;
		int resCount = 0;
		GFloat mint = std::numeric_limits<GFloat>::max();

		for (int i = 0; i < 3; ++i) {
			GFloat t, cross;
			if (GetRayTravel(origin[i], dir[i], t, cross)) {
				bool okay = true;
				for (int j = 0; j < 3; ++j) {
					if (i == j)
						continue;
					if (!Between(-0.5, origin[j] + dir[j] * t, 0.5)) {
						okay = false;
						break;
					}
				}
				if (okay) {
					++resCount;
					if (t < mint) {
						mint = t;
						crossedPlane = i;
					}
				}
			}
		}

		if (resCount > 0 && resCount <= 2)
		{
			GPoint pt = origin + dir * mint;
			GVector normal(0,0,0);
			normal[crossedPlane] = pt[crossedPlane] > 0 ? -1 : 1;
			return Intersection(this,ray,mint,pt,normal,true,resCount == 1 ? true : false);
		} else
		{
			return Intersection(this,ray,0);
		}
}

}
}
