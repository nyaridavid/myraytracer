#ifndef DOTLIGHT_HPP
#define DOTLIGHT_HPP

namespace RayTracer
{
namespace Main
{
class DotLight;
}
}

#include <list>

#include "../GraphicsBase/GInclude.hpp"

#include "Intersection.hpp"
#include "Light.hpp" // Base class: RayTracerMain::Light
#include "../RayTracerModells/Sphere.hpp"

namespace RayTracer
{
namespace Main
{

	using namespace GraphicsBase;
	using namespace RayTracer::Modells;
	
class DotLight : public Light
{
	protected:
	GPoint position;
	GFloat power;
	GColor lightcolor;
	
	GFloat att_square;
	GFloat att_lin;
	GFloat att_const;
	
	GFloat GetAttuneation(GFloat distance) const;
	//GColor GetRayObstacles(const list<Intersection>& input, float mdist) const;
	
public:
	DotLight(World* father, GPoint pos, GFloat pow = 100, GColor color = GColor(1,1,1), GFloat a_square = 1, GFloat a_lin = 0, GFloat a_const = 0);
	virtual ~DotLight();

public:
	virtual GVector GetLightDirection(const Intersection& intersection) const;
	virtual GColor GetLuminance(const Intersection& intersection) const;
};

}
}

#endif // DOTLIGHT_HPP
