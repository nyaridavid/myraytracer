#ifndef RAYTRACERMAIN_RAY_H
#define RAYTRACERMAIN_RAY_H

namespace RayTracer
{
namespace Main
{
class Ray;
}
}

#include "../GraphicsBase/GInclude.hpp"

namespace RayTracer
{
namespace Main
{
	using namespace GraphicsBase;
	
class Ray
{
private:
	GPoint start;
	GVector direction;

	int depht;
	GFloat distance;
	
public:
	Ray Shoot() const;
	Ray Shoot(GPoint start, GVector direction) const;
	Ray Impact(GPoint impactpoint) const;
	
	Ray(GPoint start, GVector direction);

	void RWOrigin(GPoint input);
	GPoint RWOrigin() const;
	GPoint& RefOrigin() { return start; }

	void RWDirection(GVector input);
	GVector RWDirection() const;
	GVector& RefDirection() { return direction; }
	
	int GetDepht() const;
	GFloat GetDistance() const;

};

}
}
#endif
