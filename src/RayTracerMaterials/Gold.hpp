#ifndef GOLD_HPP
#define GOLD_HPP

#include "../RayTracerMain/Material.hpp"

namespace RayTracer
{
namespace Materials
{
	using namespace RayTracer::Main;
	using namespace GraphicsBase;
	
class Gold : public RayTracer::Main::Material
{
public:
	Gold();
	virtual ~Gold();

};

}
}
#endif // GOLD_HPP
