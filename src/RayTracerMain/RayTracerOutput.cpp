#include "RayTracerOutput.hpp"

namespace RayTracer
{

namespace Main
{

RayTracerOutput::RayTracerOutput(World* host):world(host)
{
}

RayTracerOutput::~RayTracerOutput()
{
}

GColor RayTracerOutput::GetPixelColor(int X, int Y) const
{
	return world->GetPixel(X,Y);
}

}

}

