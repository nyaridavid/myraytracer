#include "Material.hpp"

namespace RayTracer
{
namespace Main	
{
	Material::Material()
	{
		is_reflective =false;
		is_refractive = false;
		is_prism = false;
	}
	
	GColor Material::GetN(const Intersection& input)const{return input.GetInside() ? ni : n;}
	GFloat Material::GetNf(const Intersection& input)const{return input.GetInside() ? nfi : nf;}
	GColor Material::GetK(const Intersection& input)const{return input.GetInside() ? ki : k;}
	GFloat Material::GetKf(const Intersection& input)const{return input.GetInside() ? kfi : kf;}
	GColor Material::GetF0(const Intersection& input) const{return input.GetInside() ? F0i : F0;}
	GFloat Material::GetF0f(const Intersection& input) const{return input.GetInside() ? F0fi : F0f;}
	
	void Material::RWAmbientColor(GColor input)
	{
		ka = input;
	}
	GColor Material::RWAmbientColor() const
	{
		return ka;
	}
	void Material::RWDiffuseColor(GColor input)
	{
		kd = input;
	}
	GColor Material::RWDiffuseColor() const
	{
		return kd;
	}
	void Material::RWSpecularColor(GColor input, GFloat shininess)
	{
		ks = input;
		this->shininess = shininess;
	}
	GColor Material::RWSpecularColor() const
	{
		return ks;
	}
	GFloat Material::GetShininess() const
	{
		return shininess;
	}
	void Material::RWIsReflective(bool input)
	{
		is_reflective = input;
	}
	bool Material::RWIsReflective() const
	{
		return is_reflective;
	}
	 
	void Material::RWIsRefractive(bool input)
	{
		is_refractive = input;
	}
	bool Material::RWIsRefractive() const
	{
		return is_refractive;
	}
	
	void Material::RWReReflectiveIndex(GColor input)
	{
		n = input;
		nf = (input.r+input.g+input.b)/3;
		ni = GColor(1,1,1) / n;
		nfi = 1 / nf;
		SetF0();
	}
	GColor Material::RWReReflectiveIndex() const
	{
		return n;
	}

	void Material::RWImReflectiveIndex(GColor input)
	{
		k = input;
		kf = (input.r+input.g+input.b)/3;
		ki = GColor(1,1,1) / k;
		kfi = 1 / kf;
		SetF0();
	}
	GColor Material::RWImReflectiveIndex() const
	{
		return k;
	}
	
	void Material::RWPrism(bool input)
	{
		is_prism = input;
	}
	bool Material::PWPrism() const
	{
		return is_prism;
	}
	
	void Material::SetF0()
	{
		F0 = (n - GColor(1,1,1)) * (n - GColor(1,1,1)) + k*k;
		F0 /= (n + GColor(1,1,1)) * (n + GColor(1,1,1)) + k*k;
		F0f = (F0.r + F0.g + F0.b) / 3;
		F0i = (ni - GColor(1,1,1)) * (ni - GColor(1,1,1)) + ki*ki;
		F0i /= (ni + GColor(1,1,1)) * (ni + GColor(1,1,1)) + ki*ki;
		F0fi = (F0i.r + F0i.g + F0i.b) / 3;
	}
	
	GColor Material::GetFresnelReflect(const Intersection& input) const
	{
		GColor F1;
		GFloat cosa = input.RWView() * input.GetNormal();
		GColor F0 = GetF0(input);
		for (int i = 0; i < 3; ++i)
		{
			F1[i] = (1-F0[i]) * pow(1-cosa,5);
		}
		return F0 + F1;
	}
	GColor Material::GetFresnelRefract(const Intersection& input) const
	{
		return GColor(1,1,1) - GetFresnelReflect(input);
	}
	
	GVector Material::MirrorDirection(const Intersection& input) const
	{
		return input.RWView() * -1 + input.GetNormal() * 2;
	}
	GVector Material::RefractDirection(const Intersection& input) const
	{
		GFloat cosa = input.GetNormal() * input.RWView();
		GFloat nf = GetNf(input);
		GFloat rooted = 1-(1-cosa*cosa)/(nf*nf);
		if (rooted < 0) throw rooted;
		GVector result = (input.RWView() / nf * -1) + input.GetNormal() * (cosa/nf - sqrt(rooted));
		return result; 
	}
	void Material::RefractPrism(const Intersection& input, GVector output[], bool ok[]) const
	{
		GFloat cosa = input.GetNormal() * input.RWView();
		GColor n = GetN(input);
		GFloat sqn;
		for (int i = 0; i < 3; ++i)
		{
			sqn = 1-(1-cosa*cosa)/(n[i]*n[i]);
			if (sqn > 0)
			{
				output[i] = (input.RWView() / n[i] * -1) + input.GetNormal() * (cosa/n[i] - sqrt(sqn));
				ok[i] = true;
			} else ok[i] = false;
		}
	}
	
	GColor Material::GetAmbientColor(const Intersection& input) const
	{
		return ka * input.GetIntersected()->GetFather()->GetAmbiance()->GetLuminance();
	}
	GColor Material::GetLocalColor(const Intersection& input) const
	{
		GColor result;
		Light* tmp;
		const list<Light*>* lights = input.GetIntersected()->GetFather()->GetLights();
		for (list<Light*>::const_iterator i = lights->begin(); i != lights->end(); ++i)
		{
			tmp = *i;
			GColor luminance = tmp->GetLuminance(input);
			GFloat cosln = (tmp->GetLightDirection(input)) * input.GetNormal(); if (cosln < 0) cosln = 0;
			GFloat coshn = tmp->GetHalfDirection(input) * input.GetNormal(); if (coshn < 0) coshn = 0;
			coshn = pow(coshn, shininess);
			result += luminance * (kd * cosln + ks * coshn);
		}
		return result;
	}
	GColor Material::GetGlobalColor(const Intersection& input) const {return GColor();}
	GColor Material::GetReflectedColor(const Intersection& input) const
	{
		if (!is_reflective) return GColor();
		Intersection tmp = input.GetIntersected()->GetFather()->IntersectAllNearest(
						   input.RWIntersector().Shoot(input.GetIntersection(),MirrorDirection(input)));
		if (tmp.GetValid())
		return tmp.GetIntersected()->GetColor(tmp) * GetFresnelReflect(input); else
		{
			return GColor();
		}
	}
	GColor Material::GetRefractedColor(const Intersection& input) const
	{
		if (!is_refractive) return GColor();
		if (is_prism) return GetRefractPrism(input); else return GetRefractAvg(input);

	}
	
	GColor Material::GetRefractPrism(const Intersection& input) const
	{
		GVector tmp[3];
		bool ok[3];
		RefractPrism(input,tmp,ok);
		
		GColor result;
		Intersection sect = input;
		GColor fresnel = GetFresnelRefract(input);
		
		for (int i = 0; i < 3; ++i)
		{
			if (ok[i])
			{
			sect = input.GetIntersected()->GetFather()->IntersectAllNearest(
				   input.RWIntersector().Shoot(input.GetIntersection(),tmp[i]));
			if (sect.GetValid()) result[i] = (sect.GetIntersected()->GetColor(sect) * fresnel)[i]; 
								else result[i] = 0;
			} else result[i] = 0;
		}
		return result;
	}
	GColor Material::GetRefractAvg(const Intersection& input) const
	{
		GVector refractdir;
		GColor refractmtp;
		try { 
			refractdir = RefractDirection(input);
			refractmtp = GetFresnelRefract(input); 
		} catch (GFloat a) {
			return GColor();	
		}
		Intersection tmp = input.GetIntersected()->GetFather()->IntersectAllNearest(
		input.RWIntersector().Shoot(input.GetIntersection(),refractdir));
		if (tmp.GetValid())
		return tmp.GetIntersected()->GetColor(tmp) * refractmtp; else
			return GColor();
	}
	
	GColor Material::GetSummedColor(const Intersection& input) const
	{
		return (GetAmbientColor(input) + GetLocalColor(input) + GetGlobalColor(input) + GetReflectedColor(input) + GetRefractedColor(input)).normalized();
	}
}  // namespace RayTracerMain
}
