#ifndef CUBE_HPP
#define CUBE_HPP

#include "../GraphicsBase/GPoint.hpp"
#include "../GraphicsBase/GVector.hpp"

#include "../RayTracerMain/Modell.hpp" // Base class: RayTracerMain::Modell
#include "../RayTracerMain/Intersection.hpp"
#include "../RayTracerMain/Ray.hpp"

namespace RayTracer
{
namespace Modells
{
	using namespace GraphicsBase;
	using namespace RayTracer::Main;
	
class Cube : public RayTracer::Main::Modell
{
	
public:
	Cube(World* father):Modell(new Material(),father)
	{
	}
	virtual ~Cube();

public:
	virtual Intersection IntersectCore(const Ray& ray) const;
};

}
}
#endif // CUBE_HPP
