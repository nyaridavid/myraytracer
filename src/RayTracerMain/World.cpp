#include "World.hpp"


namespace RayTracer {
namespace Main
{
	void World::AddModell(Modell* input)
	{
		modells.push_back(input);
	}
	
	void World::AddLight(Light* input)
	{
		lights.push_back(input);
	}
	
	Intersection World::IntersectAllNearest(Ray ray) const
	{
		list<Intersection> items = IntersectAll(ray);
		if (items.empty()) return Intersection();
		list<Intersection>::const_iterator i = items.begin();
		Intersection min = *i;
		++i;
		for (;i != items.end(); ++i)
		{
			GFloat minf = min.GetDistanceRaySource();
			GFloat mini = i->GetDistanceRaySource();
			if (minf > mini)
			{
				min = *i;
			}
		}
		return min;
	}
	
	list<Intersection> World::IntersectAll(Ray ray) const
	{
		list<Intersection> result;
		for (list<Modell*>::const_iterator i = modells.begin(); i != modells.end(); ++i)
		{
			Intersection tmp = (*i)->Intersect(ray);
			if (!tmp.GetValid()) continue;
			result.push_back(tmp);
		}
		return result;
	}
	
	World::~World()
	{
		for (list<Modell*>::iterator i = modells.begin(); i != modells.end(); ++i)
		{
			delete *i;
		}
		for (list<Light*>::iterator i = lights.begin(); i != lights.end(); ++i)
		{
			delete *i;
		}
	}
	int World::GetRayLimit() const
	{
		return rayLimit;
	}
	
	const list<Light*>* World::GetLights() const
	{
		return &lights;
	}
	const list<Modell*>* World::GetModells() const
	{
		return &modells;
	}

	GColor World::GetPixel(int X, int Y) const
	{
		GSize scr = eye.RWScreen().RWSize();
		if ((X < 0 || X > scr.width) || (Y < 0 || Y > scr.height)) throw X*Y;

		Intersection use = IntersectAllNearest(eye.ShootRay(GPoint(X,Y)));
		if (use.GetValid())
		{
			return use.GetIntersected()->GetColor(use).normalized();
		} else return GColor();
	}
	
	GSize World::GetScreenSize() const
	{
		return eye.RWScreen().RWSize();
	}
}
}
