#ifndef GRAPHICSBASE_G_SIZE_H
#define GRAPHICSBASE_G_SIZE_H

namespace GraphicsBase
{
class GSize
{
	public:
		int height, width;
	
	GSize(int wid = 0, int hei = 0);
};

}  // namespace GraphicsBase
#endif
