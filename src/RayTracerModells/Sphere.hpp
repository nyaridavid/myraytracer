#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "../GraphicsBase/GPoint.hpp"

#include "../RayTracerMain/Modell.hpp" // Base class: RayTracerMain::Modell
#include "../RayTracerMain/Intersection.hpp"
#include "../RayTracerMain/Ray.hpp"

namespace RayTracer
{
namespace Modells
{

	using namespace GraphicsBase;
	using namespace RayTracer::Main;
	
class Sphere : public RayTracer::Main::Modell
{
protected:
	GPoint center;
	GFloat radius;
	
public:
	Sphere(World* father, GPoint center = GPoint(), GFloat radius = 1.0f):Modell(new Material(), father),center(center),radius(radius)
	{
		
	}
	virtual ~Sphere();

public:
	virtual Intersection IntersectCore(const Ray& ray) const;
};

}
}
#endif // SPHERE_HPP
