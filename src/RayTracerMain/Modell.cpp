#include "Modell.hpp"

namespace RayTracer
{
namespace Main
{

Modell::~Modell()
{
	delete material;
}	

Intersection Modell::Intersect(const Ray& ray) const
{
	if (ray.GetDepht() > father->GetRayLimit()) return Intersection(this,ray,0);
	Intersection result;
	if (useModelView) {
	Ray tRay(ray);
		tRay.RefOrigin() = GMatrixToPoint(GPointToRowMatrix(tRay.RefOrigin()) * modelViewInverse);
		tRay.RefDirection() = GMatrixToVector(GVectorToRowMatrix(tRay.RefDirection()) * modelViewInverse);
		tRay.RefDirection().normalize();
		result = IntersectCore(tRay);
	} else {
		result = IntersectCore(ray);
	}
	if (result.GetValid()) {
		if (useModelView) {
			result.RefPoint() = GMatrixToPoint(GPointToRowMatrix(result.RefPoint()) * modelView);
			result.RefNormal() = GMatrixToVector(GVectorToRowMatrix(result.RefNormal()) * modelViewInverse.Transposed());
			result.RefNormal().normalize();
		}
		result.RWIntersector(ray.Impact(result.GetIntersection()));
		result.RWView((ray.RWOrigin() - result.GetIntersection()).normalized());
	}
	return result;
}

GColor Modell::GetColor(const Intersection& intersection) const
{
	if (intersection.GetValid())
	{
		return material->GetSummedColor(intersection);
	} else 
	{
		return GColor();
	}
}

GColor Modell::IntersectAndGetColor(const Ray& ray) const
{
	return GetColor(Intersect(ray));
}

void Modell::RWMaterial(Material input)
{
	*material = input;
}
Material Modell::RWMaterial() const
{
	return *material;
}

void Modell::SetMaterial(Material* input)
{
	delete material;
	material = input;
}

const GMatrix& Modell::RModelView() const
{
	return modelView;
}
GMatrix& Modell::RWModelView()
{
	return modelView;
}

}
}
