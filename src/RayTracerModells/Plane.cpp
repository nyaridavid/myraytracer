#include "Plane.hpp"

namespace RayTracer
{
namespace Modells
{

Plane::~Plane()
{
}

Intersection Plane::IntersectCore(const Ray& ray) const
{
		const GPoint& origin = ray.RWOrigin();
		const GVector& dir = ray.RWDirection();

		GFloat K = normal.x * base.x + normal.y * base.y + normal.z * base.z;
		GFloat U = normal * dir;
		GFloat S = normal.x * origin.x + normal.y * origin.y + normal.z * origin.z;
		if (U == 0) return Intersection(this,ray,0);
		GFloat t = (K-S)/U;
		if (t > 0)
		{
			GPoint pt = origin + dir * t;
			return Intersection(this,ray,t,pt,normal,true,dir.arc(normal).GetRadian() > M_PI/2 ? false : true);
		} else
		{
			return Intersection(this,ray,0);
		}
	/*
		GVector dir = ray.RWDirection();
		GFloat K = base.x * normal.x + base.y * normal.y + base.z * normal.z;
		GFloat C = normal.x * dir.x + normal.y * dir.y + normal.z * dir.z;
		if (C == 0) return Intersection(this,ray,0);
		GFloat t = K/C;
		if (t < 0) return Intersection(this,ray,0);
		return Intersection(this,ray,t,ray.RWOrigin() + t*ray.RWDirection(),normal,true,false);*/
}

}
}
