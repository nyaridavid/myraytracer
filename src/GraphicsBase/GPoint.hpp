#ifndef GRAPHICSBASE_G_POINT_H
#define GRAPHICSBASE_G_POINT_H

#include "GVector.hpp"

namespace Graphics
{
template<typename VAR>
class GPoint
{
	private:
	static VAR equ;
	
	public:

	VAR x, y, z;

	GPoint(VAR x0=0, VAR y0=0, VAR z0=0);
	
	bool operator==(const GPoint<VAR>& rhs) const;
	bool operator!=(const GPoint<VAR>& rhs) const;
	static void SetEquTolerance(VAR input);
	static VAR GetEquTolerance();
		
	void operator+=(const GVector<VAR>& rhs);
	GPoint<VAR> operator+(const GVector<VAR>& rhs) const;
	
	void operator-=(const GVector<VAR>& rhs);
	GPoint<VAR> operator-(const GVector<VAR>& rhs) const;
	
	GVector<VAR> operator-(const GPoint<VAR>& rhs) const;
	
	VAR& operator[](int index);
	const VAR& operator[](int index) const;
};
		
	template<typename VAR>
	VAR GPoint<VAR>::equ = 0;

	template<typename VAR>
	bool GPoint<VAR>::operator==(const GPoint<VAR>& rhs) const
	{
		VAR dif;
		
		dif = x - rhs.x; if (dif < 0) dif = -dif;
		if (dif > equ) return false;
		dif = y - rhs.y; if (dif < 0) dif = -dif;
		if (dif > equ) return false;
		dif = z - rhs.z; if (dif < 0) dif = -dif;
		if (dif > equ) return false;
		return true;
	}
	
	template<typename VAR>
	bool GPoint<VAR>::operator!=(const GPoint<VAR>& rhs) const
	{
		return !(*this == rhs);
	}

	template<typename VAR>	
	void GPoint<VAR>::SetEquTolerance(VAR input)
	{
		if (input < 0) return;
		equ = input;
	}
	
	template<typename VAR>
	VAR GPoint<VAR>::GetEquTolerance()
	{
		return equ;
	}


template<typename VAR>
GPoint<VAR>::GPoint(VAR x0, VAR y0, VAR z0):x(x0),y(y0),z(z0)
{
	
}

template<typename VAR>
void GPoint<VAR>::operator+=(const GVector<VAR>& rhs)
{
	x += rhs.x; y += rhs.y; z += rhs.z;
}

template<typename VAR>
GPoint<VAR> GPoint<VAR>::operator+(const GVector<VAR>& rhs) const {
	GPoint<VAR> result = *this;
	result += rhs;
	return result;
}

template<typename VAR>
void GPoint<VAR>::operator-=(const GVector<VAR>& rhs)
{
	x -= rhs.x; y -= rhs.y; z -= rhs.z;
}

template<typename VAR>
GPoint<VAR> GPoint<VAR>::operator-(const GVector<VAR>& rhs) const
{
	GPoint<VAR> result = *this;
	result -= rhs;
	return result;
}

template<typename VAR>
GVector<VAR> GPoint<VAR>::operator-(const GPoint<VAR>& rhs) const {
	return GVector<VAR>(x - rhs.x, y - rhs.y, z - rhs.z);
}

template<typename VAR>
VAR& GPoint<VAR>::operator[](int index)
{
	if (index == 0) return x;
	if (index == 1) return y;
	if (index == 2) return z;
	throw index;
}

template<typename VAR>
const VAR& GPoint<VAR>::operator[](int index) const
{
	if (index == 0) return x;
	if (index == 1) return y;
	if (index == 2) return z;
	throw index;
}

}  // namespace GraphicsBase
#endif
