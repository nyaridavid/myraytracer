#ifndef RAYTRACERMAIN_LIGHT_H
#define RAYTRACERMAIN_LIGHT_H

namespace RayTracer
{
namespace Main
{
class Light;
}
}

#include "../GraphicsBase/GColor.hpp"
#include "../GraphicsBase/GVector.hpp"

#include "Intersection.hpp"

namespace RayTracer
{
namespace Main	
{	
	
	using namespace GraphicsBase;
	
class Light
{
	public:
	
	virtual ~Light()
	{
		
	}

	virtual GColor GetLuminance(const Intersection& intersection) const = 0;
	virtual GVector GetLightDirection(const Intersection& intersection) const = 0;
	virtual GColor GetLightObstacles(const Intersection& intersection, GPoint lightsource) const;
	GVector GetHalfDirection(const Intersection& intersection) const;

};

}  // namespace RayTracerMain
}
#endif
