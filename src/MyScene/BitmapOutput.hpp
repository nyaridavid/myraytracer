#ifndef BITMAPOUTPUT_HPP
#define BITMAPOUTPUT_HPP

#include "../RayTracerMain/RayTracerOutput.hpp" // Base class: RayTracer::Main::RayTracerOutput
#include <iostream>
#include <time.h>
#include "../Bitmap/bitmap_image.hpp"

namespace RayTracer
{

namespace MyScene
{
	using namespace std;
	using namespace RayTracer::Main;
	using namespace GraphicsBase;

class BitmapOutput : public RayTracerOutput
{
	char* output;
protected:

public:
	BitmapOutput(World* host, char* filename);
	virtual ~BitmapOutput();

	void RenderToBitmap();
};

}

}

#endif // BITMAPOUTPUT_HPP
