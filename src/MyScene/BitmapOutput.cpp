#include "BitmapOutput.hpp"

namespace RayTracer
{

namespace MyScene
{

BitmapOutput::BitmapOutput(World* host, char* filename):RayTracerOutput(host), output(filename)
{
}

BitmapOutput::~BitmapOutput()
{
}

void BitmapOutput::RenderToBitmap()
{
	GSize tmpsize = world->GetScreenSize();
	bitmap_image output(tmpsize.width,tmpsize.height);

	#pragma omp parallel for
	for (int i = 0; i < tmpsize.width; ++i) {
		for (int j = 0; j < tmpsize.height; ++j) {
			GColor tmp = GetPixelColor (i,j);
			output.set_pixel(i,tmpsize.height - (j + 1),tmp.r*255,tmp.g*255,tmp.b*255);
		}
	}
	
	output.save_image(this->output);
}

}

}

